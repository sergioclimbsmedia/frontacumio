import { ModalContentPage } from '../pages/cliente-bono/cliente-bono';


import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
//import { HomePage } from '../pages/home/home';
import { LoginAdminPage } from '../pages/login-admin/login-admin';
import { RegistraClientePage } from '../pages/registra-cliente/registra-cliente';
import { HomeAdminPage } from '../pages/home-admin/home-admin';
import { ProtecDatAdminPage } from '../pages/protec-dat-admin/protec-dat-admin';
import { ClientePerfilPage } from '../pages/cliente-perfil/cliente-perfil';
import { ClienteBonoPage } from '../pages/cliente-bono/cliente-bono';
import { MipesoPage } from '../pages/mipeso/mipeso';

import { ClienteSeguimientoPage } from '../pages/cliente-seguimiento/cliente-seguimiento';
import { MiejerciciosPage } from '../pages/miejercicios/miejercicios';
import { SeguiDiarioDietasPage} from '../pages/segui-diario-dietas/segui-diario-dietas';
import { DiarioNuevaDietaPage } from './../pages/diario-nueva-dieta/diario-nueva-dieta';
import { NuevadietaMenuPage } from '../pages/nuevadieta-menu/nuevadieta-menu';
import { MenuDescribidoPage } from '../pages/menu-describido/menu-describido';
import { SeguiGestioncitasPage } from '../pages/segui-gestioncitas/segui-gestioncitas';
import { GestioncitasHistoryPage } from '../pages/gestioncitas-history/gestioncitas-history';
import { HistoryClinicoPage } from '../pages/history-clinico/history-clinico';
import { AjustesPage } from '../pages/ajustes/ajustes';
import { TabsPage } from '../pages/tabs/tabs';
import { ClienteHomePage } from '../pages/cliente-home/cliente-home';
import { ClienteDietaActualPage } from '../pages/cliente-dieta-actual/cliente-dieta-actual';
import { ClienteEjercicioPage } from '../pages/cliente-ejercicio/cliente-ejercicio';
import { ClienteDiarioDieteticoPage } from '../pages/cliente-diario-dietetico/cliente-diario-dietetico';
import { CalendarioPage } from '../pages/calendario/calendario';

//popovers
import { PopoverEjercicio } from './../pages/miejercicios/miejercicios';
import { PopoverPage } from './../pages/mipeso/mipeso';
import { PopoverDietas } from '../pages/segui-diario-dietas/segui-diario-dietas';
import { PopoverCalendario } from '../pages/calendario/calendario';
import { GraficosPage } from '../pages/graficos/graficos';

// graficos

import { ChartsModule } from 'ng2-charts';


@NgModule({
  declarations: [
    MyApp,
    LoginAdminPage, 
    RegistraClientePage,
    HomeAdminPage,ProtecDatAdminPage,
    ClientePerfilPage,
    ClienteBonoPage,MipesoPage,ClienteSeguimientoPage,
    MiejerciciosPage,
    SeguiDiarioDietasPage,
    DiarioNuevaDietaPage,
    NuevadietaMenuPage,
    MenuDescribidoPage,
    SeguiGestioncitasPage,
    GestioncitasHistoryPage,
    HistoryClinicoPage,AjustesPage,TabsPage,ClienteHomePage,ModalContentPage,
    ClienteDietaActualPage,
    ClienteEjercicioPage,
    ClienteDiarioDieteticoPage,CalendarioPage
    ,PopoverPage,PopoverEjercicio,
    PopoverDietas,PopoverCalendario,
    GraficosPage
 
   
  
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    ChartsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginAdminPage,
    RegistraClientePage,
    HomeAdminPage,ProtecDatAdminPage,ClientePerfilPage,
    ClienteBonoPage,MipesoPage,ClienteSeguimientoPage,
    MiejerciciosPage,
    SeguiDiarioDietasPage,
    DiarioNuevaDietaPage,
    NuevadietaMenuPage,
    MenuDescribidoPage,
    SeguiGestioncitasPage,
    GestioncitasHistoryPage,
    HistoryClinicoPage,AjustesPage,TabsPage,
    ClienteHomePage,ModalContentPage,ClienteDietaActualPage,
    ClienteEjercicioPage,
    ClienteDiarioDieteticoPage,CalendarioPage
    ,PopoverPage,PopoverEjercicio,
    PopoverDietas,PopoverCalendario,
    GraficosPage
   

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
