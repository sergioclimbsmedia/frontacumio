import { ClienteBonoPage } from './../pages/cliente-bono/cliente-bono';
import { ClienteSeguimientoPage } from './../pages/cliente-seguimiento/cliente-seguimiento';
import { DiarioNuevaDietaPage } from './../pages/diario-nueva-dieta/diario-nueva-dieta';
import { SeguiDiarioDietasPage } from './../pages/segui-diario-dietas/segui-diario-dietas';


import { MiejerciciosPage } from './../pages/miejercicios/miejercicios';
import { MipesoPage } from './../pages/mipeso/mipeso';
import { ClientePerfilPage } from './../pages/cliente-perfil/cliente-perfil';
import { ProtecDatAdminPage } from './../pages/protec-dat-admin/protec-dat-admin';


import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//import { HomePage } from '../pages/home/home';
import { LoginAdminPage } from '../pages/login-admin/login-admin';
import { NuevadietaMenuPage } from './../pages/nuevadieta-menu/nuevadieta-menu';
import { GraficosPage } from '../pages/graficos/graficos';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
 
  
  
 rootPage:any =LoginAdminPage;
 //rootPage:any = GraficosPage;
  
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

