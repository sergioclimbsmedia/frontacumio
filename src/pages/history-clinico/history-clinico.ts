import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the HistoryClinicoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-history-clinico',
  templateUrl: 'history-clinico.html',
})
export class HistoryClinicoPage {

  //checkbox
  diabetes:boolean;
  cardiopatia:boolean;
  obesidad:boolean;
  enfermedades:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryClinicoPage');
  }


  regresa(){
    this.navCtrl.pop();
  }

  confirma(){
    console.log('confirma primerpo....'+this.enfermedades);
    this.enfermedades=[];
    if(this.diabetes==true){
        this.enfermedades.push('diabetes');
    }
    if(this.cardiopatia==true){
               this.enfermedades.push('cardiopatia');
     }
    if( this.obesidad==true){
                this.enfermedades.push('obesidad');     
     }
  
    console.log('confirma ....'+this.enfermedades);

    alert("confirma historial clinico /genera pdf");
  }
  
}
