import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistoryClinicoPage } from './history-clinico';

@NgModule({
  declarations: [
    HistoryClinicoPage,
  ],
  imports: [
    IonicPageModule.forChild(HistoryClinicoPage),
  ],
})
export class HistoryClinicoPageModule {}
