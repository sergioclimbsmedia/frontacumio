import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClienteDiarioDieteticoPage } from './cliente-diario-dietetico';

@NgModule({
  declarations: [
    ClienteDiarioDieteticoPage,
  ],
  imports: [
    IonicPageModule.forChild(ClienteDiarioDieteticoPage),
  ],
})
export class ClienteDiarioDieteticoPageModule {}
