import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ClienteDiarioDieteticoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cliente-diario-dietetico',
  templateUrl: 'cliente-diario-dietetico.html',
})
export class ClienteDiarioDieteticoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClienteDiarioDieteticoPage');
  }
  dias(){
    alert("mostraremos los dias de la semana");
  }

}
