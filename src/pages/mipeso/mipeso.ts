import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController,PopoverController} from 'ionic-angular';

/**
 * Generated class for the MipesoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mipeso',
  templateUrl: 'mipeso.html',
})
export class MipesoPage {

  peso:number;

  constructor(public navCtrl: NavController, public navParams: NavParams
             ,public popoverCtrl: PopoverController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MipesoPage');
  }

  suma(){
    this.peso++;
  }

  resta(){
     this.peso--;
  }
 
  aceptaPeso(){
    alert("establecemos peso");
  }

  abrepeso(myEvent) {
    let popover = this.popoverCtrl.create(PopoverPage, {}, {cssClass: 'custom-popover'});
    popover.present({
      ev: myEvent
    });
  }

 
  regresa(){
    console.log("regresamos");
    this.navCtrl.pop();
     }

}// fin de la clase MipesoPage

// VENTANA DEL POPOVER 
@Component({
  templateUrl: 'mostrar_peso_fecha.html'

})
export class PopoverPage {
  constructor(public viewCtrl: ViewController) {}

  cierra() {
    console.log("entra aqui");
    this.viewCtrl.dismiss();
  }
}
