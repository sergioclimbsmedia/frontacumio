import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MipesoPage } from './mipeso';

@NgModule({
  declarations: [
    MipesoPage,
  ],
  imports: [
    IonicPageModule.forChild(MipesoPage),
  ],
})
export class MipesoPageModule {}
