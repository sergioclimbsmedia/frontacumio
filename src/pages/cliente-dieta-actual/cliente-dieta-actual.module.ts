import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClienteDietaActualPage } from './cliente-dieta-actual';

@NgModule({
  declarations: [
    ClienteDietaActualPage,
  ],
  imports: [
    IonicPageModule.forChild(ClienteDietaActualPage),
  ],
})
export class ClienteDietaActualPageModule {}
