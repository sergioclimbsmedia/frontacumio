import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController,PopoverController} from 'ionic-angular';

/**
 * Generated class for the CalendarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-calendario',
  templateUrl: 'calendario.html',
})
export class CalendarioPage {

  constructor(public navCtrl: NavController, public navParams: NavParams
               ,public popoverCtrl: PopoverController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CalendarioPage');
  }

  agregaHora(myEvent){
    
    let popover = this.popoverCtrl.create(PopoverCalendario,{}, {cssClass: 'popover-calendario'});
    popover.present({
      ev: myEvent
    });
  }


  regresa(){
   this.navCtrl.pop();
  }
}// fin de la clase
@Component({
  templateUrl: 'calendari_hora.html'

})
export class PopoverCalendario {
 
  constructor(public viewCtrl: ViewController,public NavParams:NavParams) {
      
  }

  cierra() {
    console.log("entra aqui");
    this.viewCtrl.dismiss();
  }
}

