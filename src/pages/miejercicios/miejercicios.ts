import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,ViewController,PopoverController } from 'ionic-angular';

/**
 * Generated class for the MiejerciciosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-miejercicios',
  templateUrl: 'miejercicios.html',
})
export class MiejerciciosPage {

  testCheckboxOpen:boolean ;
  seleccionado:boolean;
  ejerciciosMarcados:any=[];
  data:any={};
  constructor(public navCtrl: NavController, public navParams: NavParams,
                 public alertCtrl: AlertController,public popoverCtrl: PopoverController) {
  
     
  }
 

  ionViewDidLoad() {
    console.log('ionViewDidLoad MiejerciciosPage');
  
      }

  ejercicios() {
        console.log('entra aqui');
        let alert = this.alertCtrl.create();
        alert.setTitle('Que ejercicios <br> deseas Realizar ? ');
    
        alert.addInput({
          type: 'checkbox',
          label: 'Caminata',
          value: 'caminata'
          //checked: false
        });
    
        alert.addInput({
          type: 'checkbox',
          label: 'Correr',
          value: 'correr'
        });
      
        alert.addInput({
          type: 'checkbox',
          label: 'Natacion',
          value: 'natacion'
          //checked: false
        });
    
        alert.addInput({
          type: 'checkbox',
          label: 'Musculacion',
          value: 'musculacion'
        });



        alert.addButton('Cancel');
        alert.addButton({
          text: 'Hecho',
          handler: data => {
            console.log('Checkbox data:', data);
            this.testCheckboxOpen = false;
            this.ejerciciosMarcados = data;
            this.seleccionado=true;
            console.log('----'+this.ejerciciosMarcados);
          }
        });
        alert.present();
       } // fin de la funcion de la ventana de alert

    

      
        quitar_eje(ejer){

          this.ejerciciosMarcados.forEach(element => {
  
 
              if(element==ejer){
              var i = this.ejerciciosMarcados.indexOf(ejer);
              this.ejerciciosMarcados.splice( i, 1 );
               }
         
          });
        } // fin de quitar ejercicio

      confirma(){
        alert('confirmamos todos los ejercicios con sus fechas y horas');
      }

      regresa(){
        this.navCtrl.pop();
      }
      agre_dia_hora(myEvent,ejerci){
        this.data = {data_key:'your_value'};
        console.log("- "+myEvent);
        console.log("- "+ejerci);
        let popover = this.popoverCtrl.create(PopoverEjercicio,this.data, {cssClass: 'popover-ejercicio'});
        popover.present({
          ev: myEvent
        });
      }

    }

    @Component({
      templateUrl: 'ejercicio_peso_fecha.html'
    
    })
    export class PopoverEjercicio {

      peso:number;
      valor:any;

      constructor(public viewCtrl: ViewController,public NavParams:NavParams) {
       this.valor = this.NavParams.get('data_key');
        this.peso=34;
         console.log(this.valor+"---"+this.peso);
      }
    
      cierra() {
        console.log("entra aqui");
        this.viewCtrl.dismiss();
      }
    }
    