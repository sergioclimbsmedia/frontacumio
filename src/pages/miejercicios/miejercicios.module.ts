import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MiejerciciosPage } from './miejercicios';

@NgModule({
  declarations: [
    MiejerciciosPage,
  ],
  imports: [
    IonicPageModule.forChild(MiejerciciosPage),
  ],
})
export class MiejerciciosPageModule {}
