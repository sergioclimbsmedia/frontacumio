import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeguiDiarioDietasPage } from './segui-diario-dietas';

@NgModule({
  declarations: [
    SeguiDiarioDietasPage,
  ],
  imports: [
    IonicPageModule.forChild(SeguiDiarioDietasPage),
  ],
})
export class SeguiDiarioDietasPageModule {}
