import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController,PopoverController } from 'ionic-angular';
import { DiarioNuevaDietaPage } from './../diario-nueva-dieta/diario-nueva-dieta';

/**
 * Generated class for the SeguiDiarioDietasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-segui-diario-dietas',
  templateUrl: 'segui-diario-dietas.html',
})
export class SeguiDiarioDietasPage {

  constructor(public navCtrl: NavController, public navParams: NavParams
               ,public popoverCtrl: PopoverController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SeguiDiarioDietasPage');
  }
  
  
 
  

  irNuevadieta(){
    console.log("ir nueva dieta");
   this.navCtrl.push(DiarioNuevaDietaPage);
  }

  alerthistorico(myEvent){
    let popover = this.popoverCtrl.create(PopoverDietas, {}, {cssClass: 'custom-popover'});
    popover.present({
      ev: myEvent
    });
  }
  

  regresa(){
    console.log("si hay regresa");
    this.navCtrl.pop();
  }

} // fin de la clase

// VENTANA DEL POPOVER 
@Component({
  templateUrl: 'mostrar_fecha_dietas.html'

})
export class PopoverDietas{
  constructor(public viewCtrl: ViewController) {}

  cierra() {
    console.log("entra aqui");
    this.viewCtrl.dismiss();
  }
}
