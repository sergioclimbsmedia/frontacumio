import { CalendarioPage } from './../calendario/calendario';
import { RegistraClientePage } from './../registra-cliente/registra-cliente';
import { SeguiDiarioDietasPage } from './../segui-diario-dietas/segui-diario-dietas';
import { AjustesPage } from './../ajustes/ajustes';
import { HistoryClinicoPage } from './../history-clinico/history-clinico';
import { GestioncitasHistoryPage } from './../gestioncitas-history/gestioncitas-history';
import { ClienteSeguimientoPage } from './../cliente-seguimiento/cliente-seguimiento';
import { ClienteBonoPage } from './../cliente-bono/cliente-bono';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-cliente-perfil',
  templateUrl: 'cliente-perfil.html',
})
export class ClientePerfilPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClientePerfilPage');
  }

  irbonoCitas(){
    console.log("llegaste a bono citas");
    this.navCtrl.push(ClienteBonoPage);
  
  }

  modifica(){
   this.navCtrl.push(RegistraClientePage);

  }

 
  
     irHistoryClinico(){
      this.navCtrl.push(HistoryClinicoPage);
     }

     irSeguimiento(){
      this.navCtrl.push(ClienteSeguimientoPage);
    }

    irEvaluacion(){
     this.navCtrl.push(SeguiDiarioDietasPage);
    }
 
  
  irajustes(){
    console.log("llegaste a irajustes");
    this.navCtrl.push(AjustesPage);
  }
  irAgenda(){
    
    this.navCtrl.push(CalendarioPage);
  }

  aBono(){
    console.log("bien");
     this.navCtrl.push(ClienteBonoPage);
     }

 

  irGestionCitas(){
    
    this.navCtrl.push(GestioncitasHistoryPage);
    console.log("llegaste a bono citas");
  }
 
  regresa(){
    console.log("regresa");
    this.navCtrl.pop();
     }

}
