import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClienteBonoPage } from './cliente-bono';

@NgModule({
  declarations: [
    ClienteBonoPage,
  ],
  imports: [
    IonicPageModule.forChild(ClienteBonoPage),
  ],
})
export class ClienteBonoPageModule {}
