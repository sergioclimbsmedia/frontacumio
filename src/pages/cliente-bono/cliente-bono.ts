import { RegistraClientePage } from './../registra-cliente/registra-cliente';
import { Component } from '@angular/core';
import { IonicPage, NavController, Platform,NavParams,ModalController,ViewController } from 'ionic-angular';
/**
 * Generated class for the ClienteBonoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cliente-bono',
  templateUrl: 'cliente-bono.html',
})
export class ClienteBonoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
             public modalCtrl: ModalController) {
  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClienteBonoPage');
  }
  modifica(){
    this.navCtrl.push(RegistraClientePage);
 
   }
  abonar(){
    let modal = this.modalCtrl.create(ModalContentPage);
  modal.present();
   }

  regresa(){
    console.log("regresa");
    this.navCtrl.pop();
     }
    
    
}


@Component({
            templateUrl: 'modalAcreditado.html'
 /* template: `
<ion-header>
  <ion-toolbar>
    <ion-title>
      Description
    </ion-title>
    <ion-buttons start>
      <button ion-button (click)="dismiss()">
        <span ion-text color="primary" showWhen="ios">Cancel</span>
        <ion-icon name="md-close" showWhen="android, windows"></ion-icon>
      </button>
    </ion-buttons>
  </ion-toolbar>
</ion-header>
<ion-content>
  <ion-list>
      <ion-item>
        <ion-avatar item-start>
          <img src="{{character.image}}">
        </ion-avatar>
        <h2>{{character.name}}</h2>
        <p>{{character.quote}}</p>
      </ion-item>
      <ion-item *ngFor="let item of character['items']">
        {{item.title}}
        <ion-note item-end>
          {{item.note}}
        </ion-note>
      </ion-item>
  </ion-list>
</ion-content>
`*/
})

export class ModalContentPage {
  character;

  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: ViewController
  ) {
    var characters = [
      {
        name: 'Gollum',
        quote: 'Sneaky little hobbitses!',
        image: 'assets/img/avatar-gollum.jpg',
        items: [
          { title: 'Race', note: 'Hobbit' },
          { title: 'Culture', note: 'River Folk' },
          { title: 'Alter Ego', note: 'Smeagol' }
        ]
      },
      {
        name: 'Frodo',
        quote: 'Go back, Sam! I\'m going to Mordor alone!',
        image: 'assets/img/avatar-frodo.jpg',
        items: [
          { title: 'Race', note: 'Hobbit' },
          { title: 'Culture', note: 'Shire Folk' },
          { title: 'Weapon', note: 'Sting' }
        ]
      },
      {
        name: 'Samwise Gamgee',
        quote: 'What we need is a few good taters.',
        image: 'assets/img/avatar-samwise.jpg',
        items: [
          { title: 'Race', note: 'Hobbit' },
          { title: 'Culture', note: 'Shire Folk' },
          { title: 'Nickname', note: 'Sam' }
        ]
      }
    ];
    this.character = characters[1];
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}