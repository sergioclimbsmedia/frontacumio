import { ClientePerfilPage } from './../cliente-perfil/cliente-perfil';
import { ClienteBonoPage } from './../cliente-bono/cliente-bono';
import { ProtecDatAdminPage } from './../protec-dat-admin/protec-dat-admin';


import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the HomeAdminPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home-admin',
  templateUrl: 'home-admin.html',
})
export class HomeAdminPage {

   personabuscar:string;
   encontrado:boolean=false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomeAdminPage');
  }

  proteccion(){
    console.log("click en proteccion");
    this.navCtrl.push(ProtecDatAdminPage);
  }

   aBono(){
  console.log("bien");
   this.navCtrl.push(ClienteBonoPage);
   }
   aPerfilCliente(){
   console.log('nos dirigimos');
   this.navCtrl.push(ClientePerfilPage);
   }
    calendario(){
      alert('debe salir calendario');
    }

  buscamos(){
   if(this.personabuscar=="juan"){

     this.encontrado=true;

   }
 
  }

}
