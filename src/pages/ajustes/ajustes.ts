import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegistraClientePage } from '../registra-cliente/registra-cliente';

/**
 * Generated class for the AjustesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ajustes',
  templateUrl: 'ajustes.html',
})
export class AjustesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AjustesPage');
  }

  eliminar(){
    alert("eliminaremossss");
  }

   modifica(){
     console.log("modif");
     this.navCtrl.push(RegistraClientePage);
   }

  regresa(){
    this.navCtrl.pop();
  }
}
