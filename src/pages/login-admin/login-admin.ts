import { TabsPage } from './../tabs/tabs';

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

//componentes Creados 
import { HomeAdminPage } from './../home-admin/home-admin';


@IonicPage()
@Component({
  selector: 'page-login-admin',
   templateUrl: 'login-admin.html',
})
export class LoginAdminPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginAdminPage');
  }


   aHomeAdmin(){
     console.log("pinchaste cccc aqui");
     this.navCtrl.push(HomeAdminPage);
   }

    aTabs(){
     console.log("estas en cliente home");
     this.navCtrl.push(TabsPage);
   }
}
