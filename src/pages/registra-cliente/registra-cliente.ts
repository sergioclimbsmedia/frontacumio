import { ClientePerfilPage } from './../cliente-perfil/cliente-perfil';

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the RegistraClientePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registra-cliente',
  templateUrl: 'registra-cliente.html',
})
export class RegistraClientePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistraClientePage');
  }
  
  regresa(){
    this.navCtrl.pop();
     }

  irPerfil(){
       console.log("estas en perfil");
       this.navCtrl.push(ClientePerfilPage);
     }

}
