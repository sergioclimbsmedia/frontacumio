import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistraClientePage } from './registra-cliente';

@NgModule({
  declarations: [
    RegistraClientePage,
  ],
  imports: [
    IonicPageModule.forChild(RegistraClientePage),
  ],
})
export class RegistraClientePageModule {}
