import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeguiGestioncitasPage } from './segui-gestioncitas';

@NgModule({
  declarations: [
    SeguiGestioncitasPage,
  ],
  imports: [
    IonicPageModule.forChild(SeguiGestioncitasPage),
  ],
})
export class SeguiGestioncitasPageModule {}
