import { GestioncitasHistoryPage } from './../gestioncitas-history/gestioncitas-history';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SeguiGestioncitasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-segui-gestioncitas',
  templateUrl: 'segui-gestioncitas.html',
})
export class SeguiGestioncitasPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SeguiGestioncitasPage');
  }

  irNuevaCita(){
    alert(" calendario");
    }
    
  historicoCitas(){
  this.navCtrl.push(GestioncitasHistoryPage);
  }

  

  regresa(){
    this.navCtrl.pop();
  }

}
