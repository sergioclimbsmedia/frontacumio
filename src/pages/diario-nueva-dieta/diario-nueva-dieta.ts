import { NuevadietaMenuPage } from './../nuevadieta-menu/nuevadieta-menu';

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DiarioNuevaDietaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-diario-nueva-dieta',
  templateUrl: 'diario-nueva-dieta.html',
})
export class DiarioNuevaDietaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DiarioNuevaDietaPage');
  }
  iraMenu(){
    console.log("ir menu");

   this.navCtrl.push(NuevadietaMenuPage);
   
  }
  irMenuproteina(){
alert(" estas en menuproteina");
  }
  irMenucena(){
alert(" estas en menucena");
  }
  irDietactual(){
alert(" estas en  ir dietactual");
  }

  regresa(){
    this.navCtrl.pop();
  }

}
