import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DiarioNuevaDietaPage } from './diario-nueva-dieta';

@NgModule({
  declarations: [
    DiarioNuevaDietaPage,
  ],
  imports: [
    IonicPageModule.forChild(DiarioNuevaDietaPage),
  ],
})
export class DiarioNuevaDietaPageModule {}
