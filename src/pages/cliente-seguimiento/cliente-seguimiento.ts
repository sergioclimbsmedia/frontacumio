import { SeguiGestioncitasPage } from './../segui-gestioncitas/segui-gestioncitas';
import { SeguiDiarioDietasPage } from './../segui-diario-dietas/segui-diario-dietas';
import { MiejerciciosPage } from './../miejercicios/miejercicios';
import { MipesoPage } from './../mipeso/mipeso';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ClienteSeguimientoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cliente-seguimiento',
  templateUrl: 'cliente-seguimiento.html',
})
export class ClienteSeguimientoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClienteSeguimientoPage');
  }

  regresa(){
    this.navCtrl.pop();
     }

     irMipeso(){
       this.navCtrl.push(MipesoPage);
     }
     
     irMiejercicio(){
      this.navCtrl.push(MiejerciciosPage);
     }
     irGestioncita(){
      console.log("gestion cita");
      
      this.navCtrl.push(SeguiGestioncitasPage);
    }
     irMidiario(){
       console.log("heyyyy");
       this.navCtrl.push(SeguiDiarioDietasPage);
     }

    
}
