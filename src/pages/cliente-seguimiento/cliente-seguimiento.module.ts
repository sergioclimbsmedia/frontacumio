import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClienteSeguimientoPage } from './cliente-seguimiento';

@NgModule({
  declarations: [
    ClienteSeguimientoPage,
  ],
  imports: [
    IonicPageModule.forChild(ClienteSeguimientoPage),
  ],
})
export class ClienteSeguimientoPageModule {}
