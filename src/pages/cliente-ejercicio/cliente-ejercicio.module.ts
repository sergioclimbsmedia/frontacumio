import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClienteEjercicioPage } from './cliente-ejercicio';

@NgModule({
  declarations: [
    ClienteEjercicioPage,
  ],
  imports: [
    IonicPageModule.forChild(ClienteEjercicioPage),
  ],
})
export class ClienteEjercicioPageModule {}
