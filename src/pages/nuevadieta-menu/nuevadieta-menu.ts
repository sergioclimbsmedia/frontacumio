import { MenuDescribidoPage } from './../menu-describido/menu-describido';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the NuevadietaMenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-nuevadieta-menu',
  templateUrl: 'nuevadieta-menu.html',
})
export class NuevadietaMenuPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NuevadietaMenuPage');
  }

  sacarMenu(){
    this.navCtrl.push(MenuDescribidoPage);
  }

  regresa(){
    this.navCtrl.pop();
  }

}
