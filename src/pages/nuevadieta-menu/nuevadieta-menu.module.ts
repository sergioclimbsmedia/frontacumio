import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NuevadietaMenuPage } from './nuevadieta-menu';

@NgModule({
  declarations: [
    NuevadietaMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(NuevadietaMenuPage),
  ],
})
export class NuevadietaMenuPageModule {}
