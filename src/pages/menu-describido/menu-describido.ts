import { SeguiDiarioDietasPage } from './../segui-diario-dietas/segui-diario-dietas';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MenuDescribidoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu-describido',
  templateUrl: 'menu-describido.html',
})
export class MenuDescribidoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuDescribidoPage');
  }
  confirmaMenu(){
    this.navCtrl.push(SeguiDiarioDietasPage);
  }
   regresa(){
  this.navCtrl.pop();
   }
}
