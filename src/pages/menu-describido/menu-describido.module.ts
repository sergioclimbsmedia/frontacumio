import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuDescribidoPage } from './menu-describido';

@NgModule({
  declarations: [
    MenuDescribidoPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuDescribidoPage),
  ],
})
export class MenuDescribidoPageModule {}
