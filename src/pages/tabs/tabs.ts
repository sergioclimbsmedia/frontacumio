import { ClienteDiarioDieteticoPage } from './../cliente-diario-dietetico/cliente-diario-dietetico';
import { ClienteEjercicioPage } from './../cliente-ejercicio/cliente-ejercicio';
import { ClienteDietaActualPage } from './../cliente-dieta-actual/cliente-dieta-actual';
import { LoginAdminPage } from './../login-admin/login-admin';
import { MipesoPage } from './../mipeso/mipeso';
import { ClienteHomePage } from './../cliente-home/cliente-home';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  tab2=ClienteDietaActualPage;
  tab1=ClienteHomePage;
  tab3=ClienteDiarioDieteticoPage;
   tab4=MipesoPage;
   tab5=ClienteEjercicioPage;
  

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

  aLogin(){
    this.navCtrl.setRoot(LoginAdminPage);
  }

}
