import { ClienteEjercicioPage } from './../cliente-ejercicio/cliente-ejercicio';

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ClienteHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cliente-home',
  templateUrl: 'cliente-home.html',
})
export class ClienteHomePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClienteHomePage');
  }
  
  irCalendario(){
    alert("calendario");
  }

  irEjercicios(){
    console.log("ir");
    this.navCtrl.push(ClienteEjercicioPage);
  }

}
