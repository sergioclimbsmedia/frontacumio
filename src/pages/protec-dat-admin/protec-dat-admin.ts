import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegistraClientePage } from '../registra-cliente/registra-cliente';

/**
 * Generated class for the ProtecDatAdminPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-protec-dat-admin',
  templateUrl: 'protec-dat-admin.html',
})
export class ProtecDatAdminPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProtecDatAdminPage');
  }
  regresa(){
    this.navCtrl.pop();
     }

     registrar(){
       console.log("llegaste aqui");
       this.navCtrl.push(RegistraClientePage);
     }

}
