import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProtecDatAdminPage } from './protec-dat-admin';

@NgModule({
  declarations: [
    ProtecDatAdminPage,
  ],
  imports: [
    IonicPageModule.forChild(ProtecDatAdminPage),
  ],
})
export class ProtecDatAdminPageModule {}
