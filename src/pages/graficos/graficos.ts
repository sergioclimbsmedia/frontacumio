import { ChartsModule } from 'ng2-charts';
import { Component ,ViewChild, asNativeElements} from '@angular/core';
import { IonicPage, NavController, NavParams, Label } from 'ionic-angular';
import chartJs from  'chart.js';

/**
 * Generated class for the GraficosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-graficos',
  templateUrl: 'graficos.html',
})
export class GraficosPage {

 @ViewChild('barCanvas') barCanvas; // view child para comunicar componentes
 @ViewChild('lineCanvas') lineCanvas;
 @ViewChild('pieCanvas') pieCanvas;
 @ViewChild('doughnutCanvas') doughnutCanvas;


    barChart:any;
    lineChart:any;
    pieChart:any;
    doughnutChart:any;
    val1:number;
    val2:number;
    val3:number;


  constructor(public navCtrl: NavController, public navParams: NavParams) {
    
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad GraficosPage');
  }

   ngAfterViewInit(){

    setTimeout(()=>{
      this.barChart=this.getBarChart();
      this.lineChart=this.getLineChart();
    },2000)
   }

  getChart(context,chartType,data,options){

     return new chartJs(context,{
      
                data,
                options,
                type:chartType

       })
  }

  getBarChart(){
    const data={

      labels:['verde','rojo','azul'],
      datasets: [{
        label: ['votos','jugadores','tiros'],
        data:[12,25,90],
        backgroundColor:['green', 'red','blue'],
        borderWidth:1
      }]
    };

   const options={

         scales:{
            yAxes:[{
               ticks:{
                 beginAtZero:true
               }
            }]
         }
   }

    return this.getChart(this.barCanvas.nativeElement,'bar',data,options);
  }


  getLineChart(){
    this.val1= 20;
    this.val2=50 ;
    this.val3= 10;

    const data={

      labels:['enero','abril','julio'],
      datasets: [{
        label: 'menu dias',
        fill:false,
        lineTension:0.1,
        backgroundColor:'yellow',
        borderColor:'red',
        borderCapStyle:'butt',
        borderJoinStyle:'miter',
        pointRadius:1,
        pointHitRadius:10,
        data:[this.val1,this.val2,this.val3],
        scanGaps:false,
      },{
        label: 'menu2 dias',
        fill:false,
        lineTension:0.1,
        backgroundColor:'pink',
        borderColor:'blue',
        borderCapStyle:'butt',
        borderJoinStyle:'miter',
        pointRadius:1,
        pointHitRadius:10,
        data:[10,15,45],
        scanGaps:false,
       } ]
    }
     return this.getChart(this.lineCanvas.nativeElement,'line',data,{});
  }

}// fin de la clase
