import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GestioncitasHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gestioncitas-history',
  templateUrl: 'gestioncitas-history.html',
})
export class GestioncitasHistoryPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GestioncitasHistoryPage');
  }

   regresa(){
     console.log("regresa");
     this.navCtrl.pop();
   }
}
