import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GestioncitasHistoryPage } from './gestioncitas-history';

@NgModule({
  declarations: [
    GestioncitasHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(GestioncitasHistoryPage),
  ],
})
export class GestioncitasHistoryPageModule {}
